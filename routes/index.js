var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
  res.render('index', {
    title: 'Home'
  });
});

router.get('/download', function(req, res){
  res.render('download', {
    title: 'Download'
  });
});

router.get('/upload', function(req, res){
  res.render('upload', {
    title: 'Upload'
  });
});

router.get('/about', function(req, res){
  res.render('about', {
    title: 'About'
  });
});

router.get('/contact', function(req, res){
  res.render('contact', {
    title: 'Contact'
  });
});








router.get('/download/software', function(req, res){
  res.render('download/software', {
    title: 'Software'
  });
});

router.get('/download/movies', function(req, res){
  res.render('download/movies', {
    title: 'Movies'
  });
});


  router.get('/download/movies/genre', function(req, res){
    res.render('download/movies/genre', {
      title: 'Movies by Genre'
    });
  });

  router.get('/download/movies/rating', function(req, res){
    res.render('download/movies/rating', {
      title: 'Movies by Rating'
    });
  });




router.get('/download/tv', function(req, res){
  res.render('download/tv', {
    title: 'TV'
  });
});

router.get('/download/games', function(req, res){
  res.render('download/games', {
    title: 'Games'
  });
});

router.get('/download/music', function(req, res){
  res.render('download/music', {
    title: 'Music'
  });
});

router.get('/download/books', function(req, res){
  res.render('download/books', {
    title: 'Books'
  });
});

router.get('/download/books/notw', function(req, res){
  res.render('download/books/notw', {
    title: 'The Name of the Wind'
  });
});

module.exports = router;
